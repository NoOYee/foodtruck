<?php
/**
* Plugin Name: Erhvervshjemmesider.dk
* Plugin URI: http://erhvervshjemmesider.dk/
* Description: Et plugin indeholdende en række links og informationer omkring ErhvervsHjemmesider.dk
* Version: 1.0
* Author: ErhvervsHjemmesider.dk ApS
* Author URI: http://erhvervshjemmesider.dk/
* License: A "Slug" license name e.g. GPL12
*/
defined('ABSPATH') or die("Cannot access pages directly.");

add_action( 'admin_bar_menu', 'add_nodes_and_groups_to_toolbar', 50 );

function add_nodes_and_groups_to_toolbar( $wp_admin_bar ) {

	// ErhvervsHjemmesider.dk
	$args = array(
		'id'    => 'erhj_dk',
		'title' => 'ErhvervsHjemmesider.dk',
        'href'  => get_home_url() . "/wp-admin/admin.php?page=erhvervshjemmesider.dk",
	);
	$wp_admin_bar->add_node( $args );

	// Kundecenter
	$args = array(
		'id'     => 'erhj_kundecenter',
		'title'  => 'Kundecenter',
		'parent' => 'erhj_dk',
        'href'  => 'http://erhvervshjemmesider.dk/kundecenter',
        'meta' => array( 'target' => '_blank' ),
	);
	$wp_admin_bar->add_node( $args );

	// Services
	$args = array(
		'id'     => 'erhj_services',
		'title'  => 'Services',
		'parent' => 'erhj_dk',
        'href'  => 'http://erhvervshjemmesider.dk/services',
        'meta' => array( 'target' => '_blank' ),
	);
	$wp_admin_bar->add_node( $args );
    
    // Kontakt
	$args = array(
		'id'     => 'erhj_kontakt',
		'title'  => 'Kontakt',
		'parent' => 'erhj_dk',
        'href'  => 'http://erhvervshjemmesider.dk/kontakt',
        'meta' => array( 'target' => '_blank' ),
	);
	$wp_admin_bar->add_node( $args );
}


add_action( 'admin_menu', 'my_plugin_menu' );

function my_plugin_menu() {
	add_menu_page('Erhvervshjemmesider.dk', 'ERHJ.DK', 'manage_options', 'erhvervshjemmesider.dk', 'my_plugin_options', plugin_dir_url(__FILE__).'img/icon.png');
}

function my_plugin_options() {
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
?>  

<style>
#wpcontent {
background-color: white;
padding-left: 0px;
}
</style>
	<div id="erhj" class="body">
    <div class="plugin-title">
    <img class="limit-logo" src="<?php echo plugin_dir_url(__FILE__).'img/logo-erhj.png'; ?>" alt="Erhvervshjemmesider.dk logo">
    </div>   
    <div class="plugin-content">
                
    <!--Husker du os?-->
    <div class="box-spacing">
        <div class="table-cell padding"><i class="fa fa-users fa-4x" aria-hidden="true"></i>        </div>
        <div class="table-cell text-box">
            <h2>Husker du os?</h2>
            <p>Det håber vi! Vi har nemlig lavet denne hjemmeside i det fantastiske WordPress CMS. Vi bistår efterfølgende med drift, support og vedligeholdelse, såfremt du har en vedligeholdelsesaftale hos os. Du kan altid besøge os på <a href="https://www.erhvervshjemmesider.dk" target="_blank">www.erhvervshjemmesider.dk</a> og holde dig underrettet omkring, hvad vi løbende kan hjælpe dig med.</p>
        </div>
    </div>
    
    <!--Vedligeholdelse-->
    <div class="box-spacing">
        <div class="table-cell padding"><i class="fa fa-wrench fa-4x" aria-hidden="true"></i></i></i>        </div>
        <div class="table-cell text-box">
            <h2>Vedligeholdelse</h2> 
        <p>Såfremt du har en <a href="https://erhvervshjemmesider.dk/services/vedligeholdelse/" target="_blank"> vedligeholdelsesaftale</a> hos os, kan du trygt læne dig tilbage. Vi tager nemlig hånd om det meste på hjemmesiden. Har du spørgsmål til support er du altid velkommen til at kontakte vores support afdeling. Du kan oprette en ticket ved at sende en mail til support@erhvervshjemmesider.dk eller igennem vores <a href="https://www.erhvervshjemmesider.dk/kundecenter/" target="_blank">kundecenter</a> </p>
        <p>Uanset om du har en vedligeholdelsesaftale hos os eller ej, så kan du altid tilgå vores <a href="https://www.erhvervshjemmesider.dk/kundecenter/videnscenter" target="_blank">videnscenter</a> og <a href="https://www.erhvervshjemmesider.dk/kundecenter/" target="_blank">kundecenter</a>, hvor du kan finde nyttig information og hjælp til din hjemmeside. </p>
        </div>
    </div> 
        
    <!--Skal hjemmesiden opdateres?-->
    <div class="box-spacing">
        <div class="table-cell padding"><i class="fa fa-refresh fa-4x" aria-hidden="true"></i></i>        </div>
        <div class="table-cell text-box">
            <h2>Skal hjemmesiden opdateres?</h2> 
     <p>Er du ikke på vedligeholdelse, så står du selv for opdatering af systemet. Det er vigtigt, at du sørger for at alle plugins, temaer og CMS er kompatible med hinanden, at du ikke opdatere for tidligt eller for sent. Og for guds skyld - husk at tage backup af filer og database. Vi vil rekommandere, at du tager kontakt til os for at undgå, at der sker nogle fejl. </p>
        </div>
    </div> 
        
    <!--Husker du at markedsføre din hjemmeside?-->
    <div class="box-spacing">
        <div class="table-cell padding"><i class="fa fa-bullhorn fa-4x" aria-hidden="true"></i></i>        </div>
        <div class="table-cell text-box">
            <h2>Husker du at markedsføre din hjemmeside?</h2> 
        <p>Du har fået et sted, hvor dine kunder kan finde dig på internettet. Men er du synlig nok? Vi hjælper dig med online markedsføring således, at du kommer længere ud på internettet og dermed får flere henvendelser, besøgende og salg på din hjemmeside eller webshop. Vi er Google partner, hvorfor vi er eksperter i at opsætte <a href="https://www.erhvervshjemmesider.dk/services/adwords/" target="_blank">Adwords kampagner</a>. Derudover arbejder vi med markedsføring på de <a href="https://www.erhvervshjemmesider.dk/services/facebook" target="_blank">sociale medier</a> og <a href="https://www.erhvervshjemmesider.dk/services/seo/" target="_blank">søgemaskineoptimering</a> så du kommer til at ligge højt på Google.</p> 
        </div>
    </div> 
        
    <!--Udvidelser til hjemmesiden?-->
    <div class="box-spacing">
        <div class="table-cell padding"><i class="fa fa-cogs fa-4x" aria-hidden="true"></i></i>        </div>
        <div class="table-cell text-box">
             <h2>Udvidelser til hjemmesiden?</h2> 
        <p>Ønsker du nogle udvidelser til systemet eller ønsker nogle ændringer i indhold og layout? Så hjælper vi dig gerne med realisere det. Tag endelig <a href="https://erhvervshjemmesider.dk/kontakt/" target="_blank">kontakt</a> til os, så undersøger vi sagen og ser om det er muligt samt hvor lang tid det tager at lave.</p> 
        </div>
    </div> 
    
    <!--Kontakt-->
    <div class="box-spacing">
        <div class="table-cell padding"><i class="fa fa-comments-o fa-4x" aria-hidden="true"></i></i>        </div>
        <div class="table-cell text-box">
            <h2>Kontakt</h2> 
        <p>Du er altid velkommen til at tage <a href="https://www.erhvervshjemmesider.dk/kontakt/" target="_blank">kontakt</a> til os på <a href="mailto:kontakt@erhvervshjemmesider.dk?Subject=Support" target="_top">kontakt@erhvervshjemmesider.dk</a> eller telefon +45 70 40 40 50 <br>
Fortsat god brug af systemet</p>
        </div>
    </div>  
    
<div class="cta-buttons">
            <a href="https://erhvervshjemmesider.dk/services/" class="button button-primary" target="_blank"/><i class="fa fa-cogs fa-lg" aria-hidden="true"></i> Services</a>
            <a href="https://erhvervshjemmesider.dk/kundecenter/" class="button button-primary" target="_blank"/> <i class="fa fa-question-circle fa-lg" aria-hidden="true"></i> Kundecenter</a>
        <a href="https://erhvervshjemmesider.dk/kontakt/" class="button button-primary" target="_blank"/><i class="fa fa-phone fa-lg" aria-hidden="true"></i> Kontakt</a>
        <h2>Venlig hilsen</h2>
<h1>ErhvervsHjemmesider.dk ApS</h1> 
        </div>
</div>
</div>
<?php
}

function my_custom_admin_head() {
	echo '<link rel="stylesheet" type="text/css" href="'. plugin_dir_url( __FILE__ ) .'css/ehdk-style.css">';
    echo '<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">';
}
add_action( 'admin_head', 'my_custom_admin_head' );

