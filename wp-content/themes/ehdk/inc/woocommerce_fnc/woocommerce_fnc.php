<?php
class woocommerce_fnc_customtheme{
	public function __construct(){
		add_action( 'wp_head', array($this, 'product_field_checktype' ));
		add_action( 'wp_enqueue_scripts', array($this, 'custom_script' ));

		add_action('wp_ajax_nopriv_show_num_week', array($this, 'show_num_week') );
		add_action('wp_ajax_show_num_week', array($this, 'show_num_week') );

		$this->product_field_checktype();
		add_filter( 'woocommerce_add_cart_item_data', array($this, 'product_field_datestartlocation_to_cart_item'), 99, 3 );
		add_filter( 'woocommerce_get_item_data', array($this, 'product_field_datestartlocation_text_cart'), 10, 2 );
		add_action( 'woocommerce_checkout_create_order_line_item', array($this, 'product_field_datestartlocation_to_order_items'), 10, 4 );
		add_action( 'woocommerce_add_to_cart_validation', array($this, 'product_field_datestartlocation_validation'), 10, 3 );
		add_action('woocommerce_checkout_update_order_meta', array($this, 'product_field_datestartlocation_to_order_item_meta'), 10, 1 );
		add_action('restrict_manage_posts', array($this, 'product_subscription_filter'), 10, 2);
		add_filter( 'request', array($this, 'product_subscription_filter_query'),99, 1);
		add_action( 'manage_edit-shop_subscription_columns', array($this, 'product_subscription_column'), 11, 1 );
		add_action( 'manage_shop_subscription_posts_custom_column', array($this, 'product_subscription_render_column'), 3, 1 );
		add_filter( 'woocommerce_product_variation_title_include_attributes', '__return_false' );
		add_filter( 'woocommerce_is_attribute_in_product_name', '__return_false' );
		add_filter( 'woocommerce_account_menu_items', array($this, 'chane_order_menuitem_account') );
	}

	function custom_script() {
		wp_enqueue_script('woocommerce_script',get_stylesheet_directory_uri().'/js/woocommerce_script.js');
		wp_localize_script('woocommerce_script', 'woocommerce_script_ajax', array('ajaxurl' => admin_url('admin-ajax.php')));
	}

	public function location_value(){
		$location = array(
			'1' => 'Tirsdag: Kirke Værløse, Netto ved Storkekrogen',
			'2' => 'Onsdag: Værløse, Netto ved Læssevej',
			'3' => 'Torsdag: Rema1000 på Bistrupvej 82 i Birkerød',
		);
		return $location;
	}

	public function days_of_week(){
		$days = array(
			'monday' => __('Monday','the7mk2'),
			'tuesday' => __('Tuesday','the7mk2'),
			'wednesday' => __('Wednesday','the7mk2'),
			'thursday' => __('Thursday','the7mk2'),
			'friday' => __('Friday','the7mk2'),
			'saturday' => __('Saturday','the7mk2'),
			'sunday' => __('Sunday','the7mk2'),
		);
		return $days;
	}

	public function product_field_checktype(){
		global $post;
    	$product = get_product( $post->ID );
    	if($product->product_type=='subscription'){
    		add_action( 'woocommerce-product-addons_end', array($this, 'product_field_datestartlocation'), 9 );
    	}elseif($product->product_type=='variable-subscription'){
    		add_action( 'woocommerce_single_variation', array($this, 'product_field_datestartlocation'), 9 );
    	}
	}

	public function product_field_datestartlocation(){
		global $post;
    	$product = get_product( $post->ID );
    	$subscription_length = WC_Subscriptions_Product::get_length( $product );
    	$nextSunday = date("Y-m-d", strtotime('next sunday'));
		?>
		<div class="product-addon">
			<label class="addon-name"><?php _e('HVORNÅR ØNSKER DU AT STARTE DIT ABONNEMENT? *','the7mk2'); ?></label>
			<p class="form-row form-row-wide">
				<input type="text" name="start_subscription" id="start_subscription" min="<?php echo $nextSunday; ?>">
				<br>
				<?php _e('Du har valgt at få leveret mad i uge','the7mk2'); ?> <span id="number_week"></span> <i class="loading_number_week fa fa-spinner fa-spin" style="display: none;"></i>
			</p>
		</div>
		<?php
		if($subscription_length!=0 or !$subscription_length){
			$location = $this->location_value();
			$days = $this->days_of_week();
			?>
			<div class="product-addon">
				<label class="addon-name"><?php _e('Hvor vil du afhente maden? *','the7mk2'); ?></label>
					<p>
						<?php
							if($location){
							?>
							<select name="pickup_location" class="addon addon-select">
								<?php
								foreach ($location as $key => $value) {
									echo '<option value="'.$key.'">'.$value.'</option>';
								}
								?>
							</select>
							<?php
							}
						?>
					</p>
			</div>
			<?php
		}
		?>
		<?php
	}

	public function show_num_week(){
		$day = $_POST["day"];
		$time = strtotime($day);

		$week = date('W', $time); // note that ISO weeks start on Monday
    	$firstWeekOfMonth = date('W', strtotime(date('Y-01-01', $time)));
    	$number_week = 1 + ($week < $firstWeekOfMonth ? $week : $week - $firstWeekOfMonth);

		echo $number_week;

		die();
	}

	public function product_field_datestartlocation_validation($true, $product_id, $quantity){
		$start_subscription = filter_input( INPUT_POST, 'start_subscription' );
		if(empty($start_subscription)){
		    wc_add_notice(__( 'Indtast venligst hvilken uge for at starte abonnement.', 'the7mk2' ), 'error');
		    $return = false;
		}else{
			$return = $true;
		}
		return $return;
	}

	public function product_field_datestartlocation_to_cart_item($cart_item_data, $product_id, $variation_id){
		$start_subscription = filter_input( INPUT_POST, 'start_subscription' );
		$lastSunday = date("Y-m-d", strtotime("last Sunday", strtotime($start_subscription)));
		$pickup_location = $_POST['pickup_location'];

		if(!empty($start_subscription)){
			$cart_item_data['start_subscription'] = $lastSunday.' '.gmdate( 'H:i:s' );
		}

		if($pickup_location!=''){
			$cart_item_data['pickup_location'] = $pickup_location;
		}
		return $cart_item_data;
	}

	public function product_field_datestartlocation_text_cart( $item_data, $cart_item ) {
	    if(!empty($cart_item['pickup_location'])){
	    	$location = $this->location_value();
	    	$pickup_location_item = $cart_item['pickup_location'];
	    	$pickup_location = $location[$pickup_location_item];
	    	$item_data[] = array(
				'key'     => __('Hvor vil du afhente maden','the7mk2'),
				'value'   => wc_clean($pickup_location),
				'display' => '',
			);
	    }
	    return $item_data;
	}

	public function product_field_datestartlocation_to_order_items($item, $cart_item_key, $values, $order){
    	if(!empty($values['pickup_location'])){
	    	$location = $this->location_value();
			$location_value = $values['pickup_location'];
	    	$pickup_location = $location[$location_value];
			$item->add_meta_data( __('Afhentningssted','the7mk2'), $pickup_location );
	    }

 		return;
	}

	public function product_field_datestartlocation_to_order_item_meta($order_id){
        global $woocommerce;
		foreach ($woocommerce->cart->cart_contents as $contents_item_key => $contents_item ) {
			$cart_pickup_location = $contents_item['pickup_location'];
	        $cart_item_variation = $contents_item['variation'];
	    }
	    if(!empty($cart_pickup_location)){
	    	update_post_meta( $order_id, 'pickup_location', $cart_pickup_location );
	    }
	    if(!empty($cart_item_variation)){
        	$location = $this->location_value();
	    	foreach($cart_item_variation as $key => $value){
			    update_post_meta( $order_id, $key, $value );
	    	}
        }
	}

	public function product_subscription_filter($post_type, $which){
		if($post_type=='shop_subscription'){
			$all_attributes = wc_get_attribute_taxonomies();
			if($all_attributes) {
				foreach($all_attributes as $attribute){
			 		?>
					<select name="<?php echo $attribute->attribute_name; ?>" id="dropdown_<?php echo $attribute->attribute_name; ?>">
						<option value=""><?php echo sprintf(__('Any %s','the7mk2'), $attribute->attribute_label); ?></option>
						<?php
						$attribute_childs = get_terms( wc_attribute_taxonomy_name($attribute->attribute_name));
						foreach($attribute_childs as $attribute_child){
							if($attribute->attribute_name == 'adult'){
								if($_GET['adult']==$attribute_child->name){
									$selected = 'selected';
								}else{
									$selected = '';
								}
							}
							if($attribute->attribute_name == 'children'){
								if($_GET['children']==$attribute_child->name){
									$selected = 'selected';
								}else{
									$selected = '';
								}
							}
							?>
							<option value="<?php echo $attribute_child->name; ?>" <?php echo $selected; ?>><?php echo $attribute_child->name; ?></option>
							<?php
						}
						?>
		        	</select>
		        <?php
		    	}
		    }
			$location = $this->location_value();
			if(!empty($location)){
				?>
				<select name="pickup_location" id="dropdown_pickup_location">
					<option value=""><?php _e('Any Pickup location','the7mk2'); ?></option>
					<?php
					foreach ($location as $key => $value) {
						if($_GET['pickup_location']==$key){
							$selected = 'selected';
						}else{
							$selected = '';
						}
						?>
						<option value="<?php echo $key; ?>" <?php echo $selected; ?> ><?php echo $value; ?></option>
						<?php
					}
					?>
				</select>
				<?php
			}
		}
	}

	public function product_subscription_filter_query($vars){
		global $typenow;

		if ( 'shop_subscription' === $typenow ) {
			if(!empty($_GET['adult'])){
				$vars['meta_query']['relation'] = 'AND';
				$vars['meta_query'][] = array(
					'key'   => 'attribute_pa_adult',
					'value' => (int) $_GET['adult'],
					'compare' => '=',
				);
			}
			if(!empty($_GET['children'])){
				$vars['meta_query']['relation'] = 'AND';
				$vars['meta_query'][] = array(
					'key'   => 'attribute_pa_children',
					'value' => (int) $_GET['children'],
					'compare' => '=',
				);
			}
			if(!empty($_GET['pickup_location'])){
				$vars['meta_query']['relation'] = 'AND';
				$vars['meta_query'][] = array(
					'key'   => 'pickup_location',
					'value' => (int) $_GET['pickup_location'],
					'compare' => 'LIKE',
				);
			}
		}
		return $vars;
	}

	public function product_subscription_column($columns){
    	$new_column = array();
    	foreach($columns as $key => $value) {
	        if($key=='order_items'){
	        	$new_column[$key]=$value;
	           $new_column['aduth_meta_column'] = __('Aduth','the7mk2');
	           $new_column['children_meta_column'] = __('Children','the7mk2');
	        }
	        $new_column[$key]=$value;
	    }

    	return $new_column;
	}

	public function product_subscription_render_column($column){
		global $post, $the_subscription, $wp_list_table;
		switch($column){
			case 'aduth_meta_column' :
				$aduth = get_post_meta( $post->ID, 'attribute_pa_adult', true );
            	echo $aduth;
				break;
			case 'children_meta_column' :
				$children = get_post_meta( $post->ID, 'attribute_pa_children', true );
            	echo $children;
				break;
		}
	}

	public function chane_order_menuitem_account( $menu_items ){

		unset( $menu_items['subscriptions'] );
	 	if ( array_key_exists( 'dashboard', $menu_items ) ) {
   			$menu_items = wcs_array_insert_after( 'dashboard', $menu_items, 'subscriptions', __( 'Subscriptions', 'woocommerce-subscriptions' ) );
  		}
		return $menu_items;
	}
}
new woocommerce_fnc_customtheme();
?>
