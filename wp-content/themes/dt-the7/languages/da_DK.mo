��    �      �      �      �  	   �     �  	   	  	   	     	     ,	     4	     K	     [	     a	     o	     �	     �	     �	     �	     �	     �	     �	     �	     	
     
     
     %
     8
     I
     \
  
   a
     l
     t
     y
     �
  
   �
     �
  	   �
     �
  	   �
     �
     �
  �   �
  W   y     �  	   �  	   �     �  a        g     m     �     �  
   �  
   �  	   �     �  
   �     �     �  =   �  %   3     Y     h     v     �     �     �  
   �     �     �     �     �     �     �     �     �  "        &     /     <     E     S     d     p     w     �     �     �     �     �     �     �       \        z     �     �     �  	   �  (   �  (   �               *     3     G  	   a     k  	   �     �     �     �     �      �  9   �  	   )  	   3  )   =  
   g     r     �  &   �     �  +   �  8     *   >  
   i     t     �     �  
   �     �     �     �     �  x   �     m  "   �     �     �     �     �     �  �       �     �     �     �     �     �     �                "     +     >     T     c     l  '   o     �  	   �     �  
   �     �  	   �     �     �               #     *     7     ?     K  
   Q     \     i     x     ~     �     �  �   �  W   ?     �     �     �  "   �  b   �     N     V     k     r  	   �     �  	   �     �     �     �     �  <   �          6     H     V     b     q     �     �     �     �     �     �     �     �     �     �     �     �       	             /     D     M     S  $   a     �     �     �     �     �     �     �  T        a     t     �     �     �  Q   �  Q         R  )   X  
   �     �     �     �     �     �  
   �     �  
     +     &   G  4   n     �     �  '   �  	   �     �     �     
      '   7   4   6   l      �      �      �      �      �      �      �      �      !     !  c   !     w!     �!  
   �!     �!  	   �!  	   �!     �!   1 Comment Apply Coupon Archives: Ascending Author Archives: %s Author: Available on backorder Billing Address By %s Category "%s" Category Archives: %s Category: Categories: Checkout Checkout Details City Click here to enter your code Click here to login Comment Comments are closed. Company Country Coupon code Create an account? Customer Details Daily Archives: %s Date Descending Details Edit Email &#42; Email address Email&#42; Find us on: Go to Top Go! Grid view Have a coupon? Home If you have shopped with us before, please enter your details in the boxes below. If you are a new customer, please proceed to the Billing &amp; Shipping section. It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help. Leave a comment List view Load more Log in to Reply Logged in as <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Log out?</a> Login Lost your password? Message Monthly Archives: %s My Account Name &#42; Name&#42; Next Next post: No products in the cart. Nothing Found One or more fields have an error. Please check and try again. Oops! That page can&rsquo;t be found. Order Complete Order Details Order Number Out Of Stock Page not found Password Password * Pay Payment Method Phone: Previous Previous post: Price Product Product details Product quantity input tooltipQty Quantity Refresh Cart Register Related Posts Related products Remember me Remove Remove this item Required fields are marked %s Results for "%s" Returning customer? SKU: Search Results for: %s Search: Ship to a different address? Shipping Address Sorry, but nothing matched your search terms. Please try again with some different keywords. Sort by date Sort by name Submit Tag Archives: %s Telephone Thank you! Your order has been received. Thank you. Your order has been received. Total Type and hit enter &hellip; Username Username or email * Username or email address View Cart View all posts by %s View cart Website Yearly Archives: %s You are here: You may also like&hellip; You may be interested in&hellip; You must be <a href="%s">logged in</a> to post a comment. Your Cart Your cart Your email address will not be published. Your order backend metaboxCategory by authorby %s comments title1 Comment %1$s Comments details buttonDetails feedbackSorry, we suspect that you are bot feedback msgFeedback has been sent to the administrator feedback msgThe message has not been sent mailCity: mailCompany: mailCountry: mailMessage: mailName: mailTelephone: mailWebsite: postCategory: shortcode simple loginLog In shortcode simple loginLogged in as <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Log out?</a> shortcode simple loginPassword shortcode simple loginRemember Me shortcode simple loginUsername submit theme-optionsAuthor widgetCategory: widgetSort by: Project-Id-Version: The7mk2 v5.0.0.b5
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-08-30 18:57+0000
PO-Revision-Date: 2017-08-30 19:05+0000
Last-Translator: ehdk <udvikler@erhvervshjemmesider.dk>
Language-Team: Danish
Language: da-DK
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1
X-Generator: Loco - https://localise.biz/
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2
X-Poedit-Basepath: ../
X-Textdomain-Support: yes
X-Poedit-SearchPath-0: . 1 Kommentar Tilføj rabatkode Akriv: Ældste Forfatter arkiv: %s Forfatter:  Kan købes på restordre Faktureringsadresse Skrevet af %s Kategori Kategori arkiv: %s Kategori: Kategorier: Gå til kassen Betaling By Klik her for at indtaste din rabatkode. Klik her for at logge ind. Kommentar Kommentarer er lukket. Virksomhed Land Rabatkode Opret en bruger? Kundedetaljer Dagligt arkiv: %s Dato Nyeste Læs mere... Rediger Email &#42; Email Email&#42; Find os på: Gå til toppen Søg! Boksvisning Har du en rabatkode? Forside Hvis du har handlet hos os før, så indtast dine logindetaljer herunder. Hvis du er ny kunde, så fortsæt til Fakturerings- og forsendelsessektionen. Det ser ikke ud til at vi kan finde det du leder efter. Prøv eventuel med en søgning. Skriv en kommentar Listevisning Indlæs flere... Log ind for at skrive en kommentar Logget ind som <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log ud fra denne konto">Log out?</a> Log ind Mistet dit password? Besked Månedligt arkiv: %s Min konto Navn * Navn&#42; Næste Næste nyhed: Ingen produkter i kurven Intet blev fundet Et eller flere felter har en fejl. Tjek efter og prøv igen. Siden kan ikke findes. Ordre gennemført Ordredetaljer Ordrenummer Ikke på lager Siden blev ikke fundet. Adgangskode Adgangskode Betal Betalingsmetode Telefonnummer Forrige Forrige nyhed: Pris Produkt Produktdetaljer Stk. Antal Opdatér kurv Registrer Relaterede nyheder Relaterede produkter Husk mig Fjern Fjern produkt Felter markeret med %s skal udfyldes Resultater for "%s" Har du handlet her før? Varenummer: Søgeresultater for: %s Søg: Send til en anden adresse? Forsendelsesadresse Der blev ikke fundet noget noget på din søgning. Prøv igen med en anden søgning. Sortér efter dato Sortér efter navn Send besked Tag arkiv: %s Telefonnumer Tillykke med dit køb! Din ordre er modtaget og er i gang med at blive behandlet. Tillykke med dit køb! Din ordre er modtaget og er i gang med at blive behandlet. I alt Skriv dit søgeord og tryk enter &hellip; Brugernavn Brugernavn eller email * Brugernavn eller email adresse Se kurv Se alle nyheder skrevet af %s Se kurv Hjemmeside Årligt arkiv: %s Du er her: Du kunne også være interesseret i&hellip; Du kunne også være interesseret i... Du skal være logget ind for at skrive en kommentar. Indkøbskurv Din indkøbskurv Din email adresse vil ikke blive vist.  Din ordre Kategori skrevet af %s 1 Kommentar %1$s Kommentarer Læs mere... Vi formoder du er en robot. Din besked blev ikke sendt. Tak for din besked. Vi vender tilbage hurtigst muligt. Beskeden blev ikke sendt. By:  Virksomhed:  Land:  Besked:  Navn:  Telefonnummer: Hjemmeside:  Kategori Log ind Logget ind som <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log ud fra denne bruger">Log out?</a> Adgangskode Husk mig Brugernavn send besked Forfatter Kategori: Sortér efter: 