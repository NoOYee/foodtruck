��          �                         U   /     �     �     �  *   �  1   �       t   #     �     �  0   �  >   �  �  ,          !     2     4  
   L  
   W     b     n  
   t          �     �     �     �   %1$s every %2$s %s week a %s-week Edit product screen, between the Billing Period and Subscription Length dropdownsfor First renewal: %s Recurring Total Recurring Totals Subscription billing period.week %s weeks Subscription lengths. e.g. "For 1 week..."1 week Subscriptions Used in the trial period dropdown. Number is in text field. 0, 2+ will need plural, 1 will need singular.week weeks every %s includes tax(Includes %s) period interval (eg "$10 _every_ 2 weeks")every period interval with ordinal number (e.g. "every 2nd"every %s Project-Id-Version: WooCommerce Subscriptions 2.2.13
Report-Msgid-Bugs-To: https://github.com/Prospress/woocommerce-subscriptions/issues
POT-Creation-Date: 2017-10-13 08:02:19+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2018-01-22 05:00+0000
Last-Translator: edamus_radar <yee@radarsofthouse.dk>
Language-Team: Danish
X-Generator: Loco - https://localise.biz/
Language: da-DK
Plural-Forms: nplurals=2; plural=n != 1 %1$s hver %2$s %s uge en %s-uge i Første opkrævning: %s Abonnement Abonnement uge %s uger 1 uge Abonnement uge uger hver %s (Inkluderer %s) hver hver %s 